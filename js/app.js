new Vue({
    el: '#app',
    created: function () {
        this.filteredUsers = this.users;
    },
    data: {
        addFormName: 'add_new',
        editFormName: 'edit',
        listFormName: 'list',
        form_type: this.listFormName,
        errors: [],
        users: [
            {
                id : 1,
                name: "leanne fraham",
                email: "FISincere@april.biz",
                avatar: "https://upload.wikimedia.org/wikipedia/commons/thumb/3/37/Small-world-network-example.png/220px-Small-world-network-example.png"
            },
            {
                id : 2,
                name: "xraham yeanne",
                email: "april@Since.com",
                avatar: "http://mxtrianz.me/wp-content/uploads/2017/05/small-photos-17-the-data-lab.jpg"
            },
            {
                id : 3,
                name: "xraham yeanne",
                email: "april@Since.com",
                avatar: "https://vignette.wikia.nocookie.net/tc201/images/0/04/Nodes.jpg/revision/latest?cb=20101118225344"
            }
        ],
        filteredUsers: [],
        current_user: {
            id : null,
            name: null,
            email: null,
            avatar: null
        }
    },
    computed: {
        totalCount: function () {
            return 'Total ' + this.filteredUsers.length + ' records found from ' + this.users.length;
        }
    },
    methods: {
        showAddForm: function () {
            this.form_type = this.addFormName;
        },
        showEditForm: function(userId) {
            this.form_type = this.editFormName;
            this.current_user = this.getUserById(userId);
        },
        showList: function () {
            this.form_type = this.listFormName;
        },
        getFormData: function(formId) {
            formId = '#' + formId;
            let id = $(formId + " input[name=id]").val();
            if (! id) {
                id = this.users.length + 1;
            }
            let name = $(formId + " input[name=name]").val();
            let email = $(formId + " input[name=email]").val();
            let avatar = $(formId + " input[name=avatar]").val();

            return { id: id, name: name, email: email, avatar: avatar }
        },
        filterByNameAndEmail: function() {
            let searchKey = $('#' + this.listFormName + " input[name=search]").val();
            this.filterUsers(searchKey);
        },
        addUser: function () {
            let user = this.getFormData(this.addFormName);
            if (this.checkForm(this.addFormName)) {
                this.storeUser(user);
                this.filterUsers('');
                this.showList();
            }
        },
        saveUser: function() {
            if (this.checkForm(this.editFormName)){
                this.filterUsers('');
                this.showList();
            }
        },
        deleteUser: function (userId, event) {
            let user = this.getUserById(userId);
            var result = confirm('Are you sure you want delete ' + user.name);
            if (result) {
                this.removeUser(userId);
            }
        },
        filterUsers: function(searchKey) {
            if (! searchKey) {
                this.filteredUsers = this.users;
                return;
            }
            searchKey = searchKey.toUpperCase();
            this.filteredUsers = this.users.filter(function(user) {
                return user.name.toUpperCase().includes(searchKey) || user.email.toUpperCase().includes(searchKey);
            });
        },
        getUserById: function(userId) {
            return this.users.find(item => item.id === userId);
        },
        storeUser: function(newUser) {
            this.users.push(newUser);
        },
        removeUser: function (userId) {
            // TODO: refactoring
            let users = this.users;
            $.each(users, function(i){
                if(users[i].id === userId) {
                    users.splice(i,1);
                    return false;
                }
            });
            this.users = users;
        },
        checkForm: function (formName) {
            this.errors = [];

            let user = this.getFormData(formName);

            if (!user.name) {
                this.errors.push("Name required.");
            }
            if (!user.email) {
                this.errors.push('Email required.');
            } else if (!this.validEmail(user.email)) {
                this.errors.push('Valid email required.');
            }
            if (!user.avatar) {
                this.errors.push("Avatar required.");
            }

            if (!this.errors.length) {
                return true;
            }
        },
        validEmail: function (email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        }
    }

});